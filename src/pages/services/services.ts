import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ListSelectedServicesPage } from '../list-selected-services/list-selected-services';

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {
  public selectedServices: Array<any> = [];
  public master;
  public selectedMastersAndServices: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.master = this.navParams.get('master');
  }

  changeSelectService(category, master, service) {
    if (service.checked) {
      this.selectedServices.push({ category: category, master: master, service: service });
    } else {
      this.selectedServices.splice(this.selectedServices.findIndex((serv) => serv.id == service.id), 1);
    }
  }

  closeSelectedService() {
    for (let i = 0; i < this.master.categories.length; i++) {
      for (let j = 0; j < this.master.categories[i].services.length; j++) {
        this.master.categories[i].services[j].checked = false;
      }
    }
    this.selectedServices = [];
  }

  pushPageListSelectedServicesPage() {
    for(let i = 0; i < this.selectedServices.length; i++){
      this.selectedMastersAndServices.push({service: this.selectedServices[i].service, master: this.selectedServices[i].master, date: null, time: null })
    }
    this.navCtrl.push(ListSelectedServicesPage, { selectedMasters: this.selectedMastersAndServices });
  }

  closePage(){
    this.navCtrl.pop();
  }
}
