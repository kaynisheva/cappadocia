import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the ReservationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reservation',
  templateUrl: 'reservation.html',
})
export class ReservationPage {
  public reservation: string = "current";
  public current;
  public past;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.current = [
      {
        id: 0,
        service:
          {
            id: 0,
            title: 'Аппаратный маникюр'
          },
        master: {
          id: 0,
          name: 'Надежда',
          surname: 'Николаева',
          avatar: 'assets/imgs/avatar.jpg'
        },
        date: '7 декабря',
        time: '11:30'
      },
      {
        id: 1,
        service:
          {
            id: 1,
            title: 'Ламинирвование ресниц'
          },
        master:
          {
            id: 1,
            name: 'Ирина',
            surname: 'Сергеева',
            avatar: 'assets/imgs/avatar.jpg'
          },
        date: '5 декабря',
        time: '15:00'
      },
      {
        id: 2,
        service:
          {
            id: 2,
            title: 'Наращивание ресниц'
          },
        master:
          {
            id: 1,
            name: 'Ирина',
            surname: 'Сергеева',
            avatar: 'assets/imgs/avatar.jpg'
          },
        date: '5 декабря',
        time: '14:00'
      }
    ];
    this.past = [
      {
        id: 0,
        service:
          {
            id: 0,
            title: 'Аппаратный маникюр'
          },
        master:
          {
            id: 0,
            name: 'Надежда',
            surname: 'Николаева',
            avatar: 'assets/imgs/avatar.jpg'
          },
        date: '7 декабря',
        time: '11:30'
      },
      {
        id: 1,
        service:
          {
            id: 1,
            title: 'Ламинирвование ресниц'
          },
        master:
          {
            id: 1,
            name: 'Ирина',
            surname: 'Сергеева',
            avatar: 'assets/imgs/avatar.jpg'
          },
        date: '5 декабря',
        time: '15:00'
      },
      {
        id: 2,
        service:
          {
            id: 2,
            title: 'наращивание ресниц'
          },
        master:
          {
            id: 1,
            name: 'Ирина',
            surname: 'Сергеева',
            avatar: 'assets/imgs/avatar.jpg'

          },
        date: '5 декабря',
        time: '14:00'
      }
    ]
  }
}
