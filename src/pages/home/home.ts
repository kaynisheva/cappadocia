import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

import { ListServicesPage } from '../list-services/list-services'
import { ListMastersPage } from '../list-masters/list-masters'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  public publicationList = [];
  public publications = [
    {
      id: 0,
      user: {
        avatar: "assets/imgs/avatar.jpg",
        name: "Николаева Ирина",
        company: "Салон красоты \"Корица\""
      },
      previewImage: "assets/imgs/1.jpg",
      description: "Кератиновое выпрямление, ламинирование волос.\n" +
      "Эффективное восстановление волос.",
      location: {
        district: "Оренбург, Центральный район",
        address: "ул. 8 марта, 21/2"
      },
      favorite: false,
      comments: ["1", "2", "3"],
      datetime: "Вчера"
    },
    {
      id: 1,
      user: {
        avatar: "assets/imgs/avatar.jpg",
        name: "Николаева Ирина",
        company: "Салон красоты \"Корица\""
      },
      previewImage: "assets/imgs/2.jpg",
      description: "Кератиновое выпрямление, ламинирование волос.\n" +
      "Эффективное восстановление волос.",
      location: {
        district: "Оренбург, Центральный район",
        address: "ул. 8 марта, 21/2"
      },
      favorite: false,
      comments: ["1", "2", "3"],
      datetime: "Вчера"
    },
    {
      id: 2,
      user: {
        avatar: "assets/imgs/avatar.jpg",
        name: "Николаева Ирина",
        company: "Салон красоты \"Корица\""
      },
      previewImage: "assets/imgs/3.jpg",
      description: "Кератиновое выпрямление, ламинирование волос.\n" +
      "Эффективное восстановление волос.",
      location: {
        district: "Оренбург, Центральный район",
        address: "ул. 8 марта, 21/2"
      },
      favorite: false,
      comments: ["1", "2", "3"],
      datetime: "Вчера"
    }
  ];

  constructor(public modalCtrl: ModalController, public navCtrl: NavController, private callNumber: CallNumber) {
    for (let i = 0; i < 3; i++) {
      let publication = Object.create(this.publications[i]);
      publication.id = this.publicationList.length;
      this.publicationList.push( publication );
    }
  }

  pushPageListServices(){
    this.navCtrl.push(ListServicesPage);
  }

  pushPageListMasters(){
    this.navCtrl.push(ListMastersPage);
  }

  clickFavorite(id) {
    this.publicationList[id].favorite = !this.publicationList[id].favorite;
  }


  doRefresh(refresher) {
    setTimeout(() => {
      this.publicationList = [];
      for (let i = 0; i < 3; i++) {
        let publication = Object.create(this.publications[i]);
        publication.id = this.publicationList.length;
        this.publicationList.push( publication );
      }
      refresher.complete();
    }, 2000);
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      for (let i = 0; i < 3; i++) {
        let publication = Object.create(this.publications[i]);
        publication.id = this.publicationList.length;
        this.publicationList.push( publication );
      }

      infiniteScroll.complete();
    }, 500);
  }

  call(){
    this.callNumber.callNumber("123456", true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }
}
