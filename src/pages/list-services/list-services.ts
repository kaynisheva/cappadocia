import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MastersPage } from '../masters/masters'

@IonicPage()
@Component({
  selector: 'page-list-services',
  templateUrl: 'list-services.html',
})
export class ListServicesPage {
  public selectedServices: Array<any> = [];
  public categories;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.categories = [
      {
        id: 0,
        title: 'Косметология',
        services: [
          {
            id: 0,
            title: 'Кератиновое выпремление волос',
            price: 'от 3000 руб.',
            duration: '45 минут',
            masters: [
              {
                id: 0,
                name: 'Александра',
                surname: 'Заельская',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 1,
                name: 'Надежда',
                surname: 'Николаева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 2,
                name: 'Ангелина',
                surname: 'Иванова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 3,
                name: 'Ирина',
                surname: 'Крючкова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 4,
                name: 'Евгения',
                surname: 'Петрова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 5,
                name: 'Екатерина',
                surname: 'Сергеева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              }
            ],
            checked: false
          },
          {
            id: 1,
            title: 'Ламинирование волос',
            price: 'от 3000 руб.',
            duration: '45 минут',
            masters: [
              {
                id: 0,
                name: 'Александра',
                surname: 'Заельская',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 1,
                name: 'Надежда',
                surname: 'Николаева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 2,
                name: 'Ангелина',
                surname: 'Иванова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 3,
                name: 'Ирина',
                surname: 'Крючкова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 4,
                name: 'Евгения',
                surname: 'Петрова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 5,
                name: 'Екатерина',
                surname: 'Сергеева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              }
            ],
            checked: false
          },
          {
            id: 2,
            title: 'Экспресс-восстановление волос',
            price: 'от 3000 руб.',
            duration: '45 минут',
            masters: [
              {
                id: 0,
                name: 'Александра',
                surname: 'Заельская',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 1,
                name: 'Надежда',
                surname: 'Николаева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 2,
                name: 'Ангелина',
                surname: 'Иванова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 3,
                name: 'Ирина',
                surname: 'Крючкова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 4,
                name: 'Евгения',
                surname: 'Петрова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 5,
                name: 'Екатерина',
                surname: 'Сергеева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              }
            ],
            checked: false
          },
        ]
      },
      {
        id: 1,
        title: 'Маникюр',
        services: [
          {
            id: 3,
            title: 'Аппаратный маникюр',
            price: 'от 3000 руб.',
            duration: '45 минут',
            masters: [
              {
                id: 0,
                name: 'Александра',
                surname: 'Заельская',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 1,
                name: 'Надежда',
                surname: 'Николаева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 2,
                name: 'Ангелина',
                surname: 'Иванова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 3,
                name: 'Ирина',
                surname: 'Крючкова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 4,
                name: 'Евгения',
                surname: 'Петрова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 5,
                name: 'Екатерина',
                surname: 'Сергеева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              }
            ],
            checked: false
          },
          {
            id: 4,
            title: 'Наращивание ногтей',
            price: 'от 3000 руб.',
            duration: '45 минут',
            masters: [
              {
                id: 0,
                name: 'Александра',
                surname: 'Карамаканова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 1,
                name: 'Виктория',
                surname: 'Николаева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 2,
                name: 'Ангелина',
                surname: 'Иванова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 3,
                name: 'Ирина',
                surname: 'Крючкова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 4,
                name: 'Евгения',
                surname: 'Петрова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 5,
                name: 'Екатерина',
                surname: 'Сергеева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              }
            ],
            checked: false
          },
          {
            id: 5,
            title: 'Покрытие шель-лаком',
            price: 'от 3000 руб.',
            duration: '45 минут',
            masters: [
              {
                id: 0,
                name: 'Александра',
                surname: 'Заельская',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 1,
                name: 'Надежда',
                surname: 'Николаева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 2,
                name: 'Ангелина',
                surname: 'Иванова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 3,
                name: 'Ирина',
                surname: 'Крючкова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 4,
                name: 'Евгения',
                surname: 'Петрова',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              },
              {
                id: 5,
                name: 'Екатерина',
                surname: 'Сергеева',
                avatar: 'assets/imgs/avatar.jpg',
                selected: false
              }
            ],
            checked: false
          }
        ]
      }
    ];
  }

  pushPageMasters() {
    this.navCtrl.push(MastersPage, { selectedServices: this.selectedServices });
  }

  changeSelectService(service) {
    if (service.checked) {
      this.selectedServices.push(service);
    } else {
      this.selectedServices.splice(this.selectedServices.findIndex((serv) => serv.id == service.id), 1);
    }
  }

  closeSelectedService() {
    let k = 0;
    for(let i = 0; i < this.categories.length; i++){
      for(let j = 0; j < this.categories[i].services.length; j++){
        this.categories[i].services[j].checked = false;
        k++;
      }
    }
    this.selectedServices = [];
  }
}
