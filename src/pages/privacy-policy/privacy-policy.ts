import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from "../login/login";

@IonicPage()
@Component({
  selector: 'page-privacy-policy',
  templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  accept() {
    localStorage.setItem('privacyPolicy', JSON.stringify({ accept: true }));
    this.navCtrl.setRoot(LoginPage);
  }
}
