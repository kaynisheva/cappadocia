import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, NavParams} from 'ionic-angular';

import { HomePage } from '../home/home'
import { ReservationPage } from '../reservation/reservation'
import { ListServicesPage } from '../list-services/list-services'
import { ListMastersPage } from '../list-masters/list-masters'

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;
  primaryPages: Array<{ title: string, icon: string, component: any }>;
  secondaryPages: Array<{ title: string, icon: string, component: any }>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.primaryPages = [
      { title: 'Главная', icon: 'home', component: HomePage },
      { title: 'Наши услуги', icon: 'book', component: ListServicesPage },
      { title: 'Мастера', icon: 'people', component: ListMastersPage },
      { title: 'Мои бронирования', icon: 'bookmark', component: ReservationPage }
    ];
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
