import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ListSelectedServicesPage } from '../list-selected-services/list-selected-services';

@IonicPage()
@Component({
  selector: 'page-masters',
  templateUrl: 'masters.html',
})
export class MastersPage {
  public selectedServices;

  public selectedMasters: Array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.selectedServices = this.navParams.get('selectedServices');
  }

  changeSelectMaster(service, master) {
    for(let j = 0; j < this.selectedServices[this.selectedServices.findIndex((serv) => serv.id == service.id)].masters.length; j++){
      this.selectedServices[this.selectedServices.findIndex((serv) => serv.id == service.id)].masters[j].selected = false;
    }
    master.selected = !master.selected;
    if(master.selected) {
      if (this.selectedMasters.findIndex((serv) => serv.service.id == service.id) < 0) {
        this.selectedMasters.push({service: service, master: master, date: null, time: null });
      } else {
        this.selectedMasters.splice(this.selectedMasters.findIndex((serv) => serv.service.id == service.id), 1, {service: service, master: master, date: null, time: null });
      }
    }
  }

  closeSelectedMaster() {
    this.selectedMasters = [];
    for(let i = 0; i < this.selectedServices.length; i++){
      for(let j = 0; j < this.selectedServices[i].masters.length; j++){
        this.selectedServices[i].masters[j].selected = false;
      }
    }
  }

  pushPageListSelectedServicesPage() {
    this.navCtrl.push(ListSelectedServicesPage, { selectedMasters: this.selectedMasters });
  }

  closePage(){
    this.navCtrl.pop();
  }
}
