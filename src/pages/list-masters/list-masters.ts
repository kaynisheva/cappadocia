import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServicesPage } from '../services/services'

@IonicPage()
@Component({
  selector: 'page-list-masters',
  templateUrl: 'list-masters.html',
})
export class ListMastersPage {
  public masters;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.masters = [
      {
        id: 0,
        name: 'Александра',
        surname: 'Заельская',
        avatar: 'assets/imgs/avatar.jpg',
        categories: [
          {
            id: 0,
            title: 'Косметология',
            services: [
              {
                id: 0,
                title: "Кератиновое выпрямление волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: "Ламинирование волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          },
          {
            id: 1,
            title: 'Ресницы',
            services: [
              {
                id: 0,
                title: 'Наращивание ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: 'Ламинирование ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          }
        ]
      },
      {
        id: 1,
        name: 'Надежда',
        surname: 'Николаева',
        avatar: 'assets/imgs/avatar.jpg',
        categories: [
          {
            id: 0,
            title: 'Косметология',
            services: [
              {
                id: 0,
                title: "Кератиновое выпрямление волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: "Ламинирование волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 2,
                title: "Наращивание волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          },
          {
            id: 1,
            title: 'Ресницы',
            services: [
              {
                id: 0,
                title: 'Наращивание ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: 'Ламинирование ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          }
        ]
      },
      {
        id: 2,
        name: 'Ангелина',
        surname: 'Иванова',
        avatar: 'assets/imgs/avatar.jpg',
        categories: [
          {
            id: 0,
            title: 'Косметология',
            services: [
              {
                id: 0,
                title: "Кератиновое выпрямление волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: "Ламинирование волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 2,
                title: "Наращивание волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          },
          {
            id: 1,
            title: 'Ресницы',
            services: [
              {
                id: 0,
                title: 'Наращивание ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: 'Ламинирование ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          }
        ]
      },
      {
        id: 3,
        name: 'Ирина',
        surname: 'Крючкова',
        avatar: 'assets/imgs/avatar.jpg',
        categories: [
          {
            id: 0,
            title: 'Косметология',
            services: [
              {
                id: 0,
                title: "Кератиновое выпрямление волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: "Ламинирование волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 2,
                title: "Наращивание волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          },
          {
            id: 1,
            title: 'Ресницы',
            services: [
              {
                id: 0,
                title: 'Наращивание ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: 'Ламинирование ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          }
        ]
      },
      {
        id: 4,
        name: 'Евгения',
        surname: 'Петрова',
        avatar: 'assets/imgs/avatar.jpg',
        categories: [
          {
            id: 0,
            title: 'Косметология',
            services: [
              {
                id: 0,
                title: "Кератиновое выпрямление волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: "Ламинирование волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 2,
                title: "Наращивание волос",
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          },
          {
            id: 1,
            title: 'Ресницы',
            services: [
              {
                id: 0,
                title: 'Наращивание ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              },
              {
                id: 1,
                title: 'Ламинирование ресниц',
                price: 'от 3000 руб.',
                duration: '45 минут',
                checked: false
              }
            ]
          }
        ]
      }
    ];
  }

  pushPageServices(master){
    this.navCtrl.push(ServicesPage, {master: master});
  }
}
