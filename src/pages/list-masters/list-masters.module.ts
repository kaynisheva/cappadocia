import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMastersPage } from './list-masters';

@NgModule({
  declarations: [
    ListMastersPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMastersPage),
  ],
})
export class ListMastersPageModule {}
