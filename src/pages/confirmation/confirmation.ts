import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-confirmation',
  templateUrl: 'confirmation.html',
})
export class ConfirmationPage {
  public dates: Array<any> = [];
  public selectedDate = null;
  public times;
  public currentDate = new Date();
  public selectedTime = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController ) {
    this.dates[0] = {
      id: 0,
      dayMounth: this.currentDate.setDate(this.currentDate.getDate()),
      selected: false
    };
    for(let i = 1; i < 60; i++){
      this.dates[i] = {
        id: i,
        dayMounth: this.currentDate.setDate(this.currentDate.getDate() + 1),
        selected: false
      };
    }
    this.times = [
      {
        id: 0,
        time: '10:00',
        selected: false
      },
      {
        id: 1,
        time: '10:30',
        selected: false
      },
      {
        id: 2,
        time: '11:00',
        selected: false
      },
      {
        id: 3,
        time: '11:30',
        selected: false
      },
      {
        id: 4,
        time: '12:00',
        selected: false
      },
      {
        id: 5,
        time: '12:30',
        selected: false
      },
      {
        id: 6,
        time: '13:30',
        selected: false
      },
      {
        id: 7,
        time: '14:00',
        selected: false
      },
      {
        id: 8,
        time: '14:30',
        selected: false
      },
      {
        id: 9,
        time: '15:00',
        selected: false
      },
      {
        id: 10,
        time: '15:30',
        selected: false
      },
      {
        id: 11,
        time: '16:00',
        selected: false
      },
      {
        id: 12,
        time: '16:30',
        selected: false
      },
      {
        id: 13,
        time: '17:00',
        selected: false
      },
      {
        id: 14,
        time: '17:30',
        selected: false
      },
      {
        id: 15,
        time: '18:00',
        selected: false
      },
      {
        id: 16,
        time: '18:30',
        selected: false
      },
      {
        id: 17,
        time: '19:00',
        selected: false
      },
      {
        id: 18,
        time: '19:30',
        selected: false
      },
      {
        id: 19,
        time: '20:00',
        selected: false
      }
    ];
  }

  changeSelectDate(date){
    for( let i = 0; i < this.dates.length; i++){
      this.dates[i].selected = false;
    }
    date.selected = !date.selected;
    if(date.selected) {
      this.selectedDate = date;
    }
  }

  changeSelectTime(time){
    for( let i = 0; i < this.times.length; i++){
      this.times[i].selected = false;
    }
    time.selected = !time.selected;
    if(time.selected) {
      this.selectedTime = time;
    }
  }

  closePage(){
    this.navCtrl.pop();
  }

  dismiss() {
    if(this.selectedDate != null && this.selectedTime != null) {
      this.viewCtrl.dismiss({selectedDate: this.selectedDate, selectedTime: this.selectedTime});
    }
  }
}
