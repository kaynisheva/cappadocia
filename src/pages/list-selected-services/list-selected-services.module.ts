import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListSelectedServicesPage } from './list-selected-services';

@NgModule({
  declarations: [
    ListSelectedServicesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListSelectedServicesPage),
  ],
})
export class ListSelectedServicesPageModule {}
