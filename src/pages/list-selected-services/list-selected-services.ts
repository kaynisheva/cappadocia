import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

import { ConfirmationPage } from '../confirmation/confirmation'

@IonicPage()
@Component({
  selector: 'page-list-selected-services',
  templateUrl: 'list-selected-services.html',
})
export class ListSelectedServicesPage {
  public selectedMastersAndServices;
  public selectedDate;
  public selectedTime;
  public selectedAllDate: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.selectedMastersAndServices =  this.navParams.get('selectedMasters');
  }

  selectTime(element){
    let modal = this.modalCtrl.create(ConfirmationPage);
    modal.onDidDismiss(data => {
      element.date = data.selectedDate.dayMounth;
      element.time = data.selectedTime.time;
    });
    modal.present();
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Бронирование',
      message: 'Вы действительно хотите подтвердить бронирование?',
      buttons: [
        {
          text: 'Отмена',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Подтвердить',
          handler: () => {
            console.log('Agree clicked');
            this.navCtrl.popToRoot();
          }
        }
      ]
    });
    confirm.present();
  }
  closePage(){
    this.navCtrl.pop();
  }
}
