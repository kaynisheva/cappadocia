import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';

import { AuthService } from '../../_services/index';

import { MenuPage } from '../menu/menu';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public step: number = 0;
  public registration: boolean = false;
  public phoneValue: string = '';
  public smsCodeValue: string = '';
  public emailValue: string = '';
  public fullnameValue: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public authService: AuthService) {
  }

  login_step0() {
    if (!/^[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s.]?[0-9]{4}$/.test(this.phoneValue)) {
      let alert = this.alertCtrl.create({
        title: 'Внимание!',
        message: 'Пожалуйста, введите нормальный номер телефона.',
        buttons: ['OK']
      });
      alert.present();

    } else {
      let confirm = this.alertCtrl.create({
        title: 'Внимание!',
        message: 'Будет использоваться следующий номер телефона: ' + this.phoneValue + '\n вы согласны или хотите изменить номер?',
        buttons: [
          {
            text: 'Изменить'
          },
          {
            text: 'Ок',
            handler: () => {
              let loader = this.loadingCtrl.create({
                content: "Подождите..."
              });
              loader.present();

              this.authService.checkPhoneNumber(this.phoneValue).subscribe(
                (result: any) => {
                  if (result.success) {

                    this.registration = result.registration;
                    loader.dismiss();
                    this.step = ++this.step;

                  } else {

                    loader.dismiss();

                    let alert = this.alertCtrl.create({
                      title: result.errorTitle,
                      message: result.errorMessage,
                      buttons: ['OK']
                    });
                    alert.present();

                  }
                }
              );
            }
          }
        ]
      });
      confirm.present();
    }
  }
  login_step1() {
    let loader = this.loadingCtrl.create({
      content: "Подождите..."
    });
    loader.present();

    this.authService.verificationPhoneNumber(this.smsCodeValue).subscribe(
      (result: any) => {
        if (result.success) {
          loader.dismiss();
          if (this.registration) {
            this.step = ++this.step;
          } else {
            this.navCtrl.setRoot(MenuPage);
          }

        } else {

          loader.dismiss();

          let alert = this.alertCtrl.create({
            title: result.errorTitle,
            message: result.errorMessage,
            buttons: ['OK']
          });
          alert.present();

        }
      }
    );
  }

  login_step2() {
    let loader = this.loadingCtrl.create({
      content: "Подождите..."
    });
    loader.present();

    this.authService.setRegistrationData(this.fullnameValue, this.emailValue).subscribe(
      (result: any) => {
        if (result.success) {
          loader.dismiss();
          this.navCtrl.setRoot(MenuPage);

        } else {

          loader.dismiss();

          let alert = this.alertCtrl.create({
            title: result.errorTitle,
            message: result.errorMessage,
            buttons: ['OK']
          });
          alert.present();
        }
      }
    );
  }

  back() {
    this.step = --this.step;
  }
}
