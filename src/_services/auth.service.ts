import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { ReplaySubject } from 'rxjs';
import { HttpErrorResponse } from "@angular/common/http";
import { apiUrl, getErrorInfo } from "../app/backend.info";

@Injectable()
export class AuthService {
  public token: string;
  public userId: number;
  public registration: boolean;
  private hashCode: string;
  private phone: string;

  constructor(private http: Http) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  checkPhoneNumber(phone: string): ReplaySubject<object> {
    let response = new ReplaySubject(1);

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers });
    let body = 'phone=' + encodeURIComponent(phone);

    this.http.post(apiUrl + '/api/v1/account/auth/login/', body, options).subscribe(
      data => {
        let body = data.json();
        this.hashCode = body.hash_code;
        this.registration = body.is_new_user;
        this.phone = phone;
        response.next({
          success: true,
          registration: this.registration
        });
      },
      (err: HttpErrorResponse) => {
        response.next(getErrorInfo(err));
      }
    );

    return response;
  }

  verificationPhoneNumber(smsCode: string): ReplaySubject<object> {
    let response = new ReplaySubject(1);

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded'});
    let options = new RequestOptions({ headers: headers });
    let body = 'sms_code=' + encodeURIComponent(smsCode) +
      '&hash_code=' + encodeURIComponent(this.hashCode);

    this.http.post(apiUrl + '/api/v1/account/auth/check-login-code/', body, options).subscribe(
      data => {
        this.token = data.json().token;
        this.userId = data.json().user_id;
        localStorage.setItem('currentUser', JSON.stringify({ phone: this.phone, token: this.token, userId: this.userId }));
        response.next({
          success: true
        });
      },
      (err: HttpErrorResponse) => {
        response.next(getErrorInfo(err));
      }
    );
    return response;
  }

  setRegistrationData(fullname: string, email: string): ReplaySubject<object> {
    let response = new ReplaySubject(1);

    let headers = new Headers({ 'Authorization': 'Token ' + this.token, 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    let body = 'full_name=' + encodeURIComponent(fullname) +
      '&email=' + encodeURIComponent(email);

    this.http.post(apiUrl + '/api/v1/account/auth/set-registration-data/', body, options).subscribe(
      data => {
        response.next({
          success: true
        });
      },
      (err: HttpErrorResponse) => {
        response.next(getErrorInfo(err));
      }
    );
    return response;
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
  }
}
