import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CallNumber } from '@ionic-native/call-number';

import { MyApp } from './app.component';
import { MenuPage } from '../pages/menu/menu';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';

import { ServicesPage } from '../pages/services/services';
import { MastersPage } from '../pages/masters/masters';
import { ReservationPage } from '../pages/reservation/reservation';
import { ConfirmationPage } from '../pages/confirmation/confirmation';
import { ListMastersPage } from '../pages/list-masters/list-masters';
import { ListServicesPage } from '../pages/list-services/list-services';
import { ListSelectedServicesPage } from '../pages/list-selected-services/list-selected-services';

import { AuthService } from '../_services/index';

@NgModule({
  declarations: [
    MyApp,
    PrivacyPolicyPage,
    LoginPage,
    MenuPage,
    HomePage,
    ReservationPage,
    ServicesPage,
    MastersPage,
    ConfirmationPage,
    ListMastersPage,
    ListServicesPage,
    ListSelectedServicesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PrivacyPolicyPage,
    LoginPage,
    MenuPage,
    HomePage,
    ReservationPage,
    ServicesPage,
    MastersPage,
    ConfirmationPage,
    ListMastersPage,
    ListServicesPage,
    ListSelectedServicesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService
  ]
})
export class AppModule {}
