import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';

import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MenuPage } from '../pages/menu/menu';

import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = PrivacyPolicyPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    if (JSON.parse(localStorage.getItem('privacyPolicy')) !== null) {
      if (JSON.parse(localStorage.getItem('currentUser')) !== null) {
        this.rootPage = MenuPage;
      } else {
        this.rootPage = LoginPage;
      }
    }
    this.rootPage = MenuPage;


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
      splashScreen.hide();
    });
  }
}

