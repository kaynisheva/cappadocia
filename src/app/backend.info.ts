export const apiUrl = 'http://85.143.214.219:8000';
export function getErrorInfo (err): Object  {
  let error = { success: false, errorCode: err.status };

  if (err.error instanceof Error) {
    return {
      success: false,
      errorTitle: 'Ошибка соединения',
      errorMessage: 'Не удалось связаться с сервером. Проверьте связь с интернетом.'
    };
  } else {

    switch (err.status) {
      case 462:
        Object.assign(error, {
          errorTitle: 'Вы уже зарегистрированны',
          errorMessage: 'Используйте свои старые учетные данные.'
        });
        break;
      case 461:
        Object.assign(error, {
          errorTitle: 'Ошибка отправки SMS',
          errorMessage: 'Возможно вы ввели неверный номер телефона.'
        });
        break;
      case 404:
        Object.assign(error, {
          errorTitle: 'Сервевер не отвечает',
          errorMessage: 'Повторите попытку позже.'
        });
        break;
      case 401:
        Object.assign(error, {
          errorTitle: 'Ошибка аутентификации',
          errorMessage: 'Пожалуйста войдите в свою учетную запись.'
        });
        break;
      case 400:
        Object.assign(error, {
          errorTitle: 'Неверный SMS код',
          errorMessage: 'Проверьте правильность кода и повторите попытку.'
        });
        break;
      default:
        Object.assign(error, {
          errorTitle: 'Упс',
          errorMessage: 'Что-то пошло не так...'
        });
    }
  }
  return error;
}
